# API tester for GraphQL <!-- omit in toc -->

JS code for test GraphQL API

<div align="center">
  <img src="img/pokespartans.png" height="30px">
  <b>Pokedex Project</b>
</div>

---

<div align="center">
	<img src="https://img.shields.io/badge/License-GPLv3-blue.svg"  alt="license badge">

</div>
<div align="center">
    <img alt="GitHub followers" src="https://img.shields.io/github/followers/eocode?label=eocode&style=social">
    <img alt="GitHub followers" src="https://img.shields.io/github/followers/KorKux1?label=KorKux1&style=social">
</div>
<div align="center">
    <img alt="Twitter URL" src="https://img.shields.io/twitter/url?label=eocode&style=social&url=https%3A%2F%2Ftwitter.com%2Feocode">
</div>

## How to clone
You can clone the repository

    $ git clone https://gitlab.com/pokespartans/backend/tester-for-graphql-api

## Installation
To install this project just type

    $ npm install

Open for test with CORS configuration:
    
    $ test_navigator.html

Test with Node axios

    $ node node_axios.js

## Preview

<div align="center">
  <img src="img/test.png">
</div>


## How to contribute

* Review the [code of conduct](https://gitlab.com/pokespartans/code-of-conduct) to contribute to the project
* You can create a Pull request to the project

## License

GNU GENERAL PUBLIC LICENSE
Version 3
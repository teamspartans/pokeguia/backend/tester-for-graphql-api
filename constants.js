module.exports = {
    apiUrl: 'https://blissful-mile-280405.ue.r.appspot.com/graphql',
    queries: {
        getPokemons: `{
          pokemons(page: 401,limit:2){
            pages
            hasNext
            hasPrev
            data{
              name
              description
              image
            }
          }
        }`,
    },
}
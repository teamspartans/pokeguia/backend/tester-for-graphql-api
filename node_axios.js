const axios_ = require('axios');
const apiUrl = require('./constants').apiUrl;
const queries = require('./constants').queries;

axios_.post(apiUrl, {
    query: queries.getPokemons
})
.then((res) => {
  console.log('--------------------- Lista de Pokemons --------------------------')
  console.log(res.data.data)
})
.catch((error) => {
  console.error(error)
});